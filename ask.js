module.exports = [
    {
        name: 'description',
        message: 'Please enter a description',
        type: 'Input',
        default: ''
    },
    {
        name: 'author',
        message: 'Please enter author',
        type: 'Input',
        default: ''
    },
    {
        name: 'license',
        message: 'Please enter license',
        type: 'Input',
        default: 'ISC'
    }
]